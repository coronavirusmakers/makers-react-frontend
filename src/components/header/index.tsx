import React, { useState } from 'react';
import logo from '../../assets/images/logoBlackOnly.png';
import './index.scss';
import { useHistory } from 'react-router-dom';
import { motion, AnimatePresence } from 'framer-motion';
import { useIntl } from 'react-intl';
import { AuthConsumer } from '../../providers/authProvider';
import { useUserData } from '../../customHooks/useUserData';
import { RoleType } from '../../models';
import { UrlManager, EntityType } from '../../helpers/helpers';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faSignOutAlt, faUser, faArchive, faCubes } from '@fortawesome/free-solid-svg-icons';

export const Header = () => {
    let history = useHistory();
    const { person } = useUserData();
    const [isOpen, setIsOpen] = useState(false);
    const intl = useIntl();
    const variants = {
        open: {
            opacity: 1,
            height: 'auto',
            transition: {
                duration: 0.3,
                when: 'beforeChildren',
                easy: 'easyOut',
            },
        },
        collapsed: {
            opacity: 0,
            height: 0,
            transition: {
                duration: 0.3,
                when: 'afterChildren',
                easy: 'easyOut',
            },
        },
    };
    const itemVariants = {
        open: (i: number) => ({
            opacity: 1,
            transition: {
                delay: i * 0.2,
                ease: 'easeOut',
            },
        }),
        collapsed: { opacity: 0 },
    };

    const closeAndGo = (route: string) => {
        setIsOpen(false);
        history.push(route);
    };
    return (
        <>
            {!!person && (
                <header className="main">
                    <div className="header">
                        <div className="logoContainer" onClick={() => closeAndGo(UrlManager(person.role).main)}>
                            <img src={logo} className="logo" alt="logo" />
                            <h1>
                                coronavirus<strong>makers</strong> App
                            </h1>
                        </div>
                        <FontAwesomeIcon
                            size="lg"
                            className="menuIcon"
                            icon={faBars}
                            onClick={() => setIsOpen((current) => !current)}
                        />
                    </div>
                    <AnimatePresence initial={false}>
                        {isOpen && (
                            <nav>
                                <motion.ul
                                    initial="collapsed"
                                    animate="open"
                                    className="menu"
                                    exit="collapsed"
                                    variants={variants}
                                    transition={{ duration: 0.3, ease: 'easeOut' }}
                                >
                                    {!!person && person.role === RoleType.Maker && (
                                        <motion.li
                                            custom={0}
                                            animate="open"
                                            initial="collapsed"
                                            variants={itemVariants}
                                            onClick={() =>
                                                closeAndGo(UrlManager(person.role, EntityType.Inventory).main)
                                            }
                                        >
                                            {' '}
                                            <FontAwesomeIcon size="lg" icon={faCubes} />
                                            {intl.formatMessage({ id: 'NAV_INVENTORY' })}
                                        </motion.li>
                                    )}
                                    <motion.li
                                        custom={1}
                                        animate="open"
                                        initial="collapsed"
                                        variants={itemVariants}
                                        onClick={() => closeAndGo(UrlManager(person.role, EntityType.Request).main)}
                                    >
                                        <FontAwesomeIcon size="lg" icon={faArchive} />
                                        {intl.formatMessage({ id: 'NAV_MATERIALS' })}
                                    </motion.li>
                                    <motion.li
                                        custom={2}
                                        animate="open"
                                        initial="collapsed"
                                        variants={itemVariants}
                                        onClick={() => closeAndGo(`/person`)}
                                    >
                                        <FontAwesomeIcon size="lg" icon={faUser} />
                                        {intl.formatMessage({ id: 'NAV_PROFILE' })}
                                    </motion.li>
                                    <AuthConsumer>
                                        {({ logout }: any) => (
                                            <motion.li
                                                custom={3}
                                                animate="open"
                                                initial="collapsed"
                                                variants={itemVariants}
                                                onClick={() => {
                                                    logout();
                                                    closeAndGo('/login');
                                                }}
                                            >
                                                <FontAwesomeIcon size="lg" icon={faSignOutAlt} />
                                                {intl.formatMessage({ id: 'NAV_LOGOUT' })}
                                            </motion.li>
                                        )}
                                    </AuthConsumer>
                                </motion.ul>
                            </nav>
                        )}
                    </AnimatePresence>
                </header>
            )}
        </>
    );
};
