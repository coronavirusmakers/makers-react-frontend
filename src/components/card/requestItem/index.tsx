import React from 'react';
import { IRequest, RoleType } from '../../../models';
import { Card } from '../index';
import { InfoItem } from '../../infoItem';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import { UrlManager, EntityType } from '../../../helpers/helpers';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQrcode } from '@fortawesome/free-solid-svg-icons';

interface IProps {
    item: IRequest;
    roleType: RoleType;
}
export enum RequestStatusType {
    Created = 'Creado',
    Preparing = 'En tránsito',
    Completed = 'Completado',
    Cancelled = 'Cancelado',
}
export const RequestItem = ({ item, roleType }: IProps) => {
    const intl = useIntl();
    const history = useHistory();
    const goToSummary = (request: IRequest) => {
        history.push(UrlManager(roleType, EntityType.Request).success, {
            data: request,
        });
    };

    const getRequestStatus = (): RequestStatusType => {
        if (item.status === 4) {
            return RequestStatusType.Cancelled;
        }
        if (item.shippings_object.length > 0) {
            let shippingsInProgress = item.shippings_object.filter((item2) => item2.status !== 2);

            if (!shippingsInProgress || (shippingsInProgress && shippingsInProgress.length === 0)) {
                return RequestStatusType.Completed;
            }
            return RequestStatusType.Preparing;
        }
        return RequestStatusType.Created;
    };

    return (
        <Card>
            <div className="info">
                <InfoItem label={intl.formatMessage({ id: 'LABEL_MATERIAL_ORDER' })} text={`${item.pk}`} />
                <InfoItem label={intl.formatMessage({ id: 'LABEL_MATERIAL_TYPE' })} text={item.resource_object.name} />

                <div className="downloadBarcode">
                    {getRequestStatus() !== RequestStatusType.Cancelled && (
                        <FontAwesomeIcon onClick={() => goToSummary(item)} icon={faQrcode} className="barcode" />
                    )}
                </div>
            </div>
            <div className="info">
                <InfoItem label={intl.formatMessage({ id: 'LABEL_QUANTITY' })} text={String(item.quantity)} />
                <InfoItem
                    label={intl.formatMessage({ id: 'LABEL_DATE' })}
                    text={`${moment(item.created_date).format('DD/MM/YYYY')}`}
                />
                <InfoItem label={intl.formatMessage({ id: 'LABEL_STATUS' })} text={getRequestStatus()} />
            </div>
            <div className="margin-s"></div>
        </Card>
    );
};
