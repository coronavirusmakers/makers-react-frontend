import React from 'react';
import { Card } from '../index';
import { InfoItem } from '../../infoItem';
import { useIntl } from 'react-intl';
import { motion } from 'framer-motion';
import { IResource, RoleType } from '../../../models';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { Button } from '../../button';

interface IProps {
    item: IResource;
    action: (id: string) => void;
    isSelected: boolean;
    hasAlert?: boolean;
    role: RoleType;
    continueAction: () => void;
}

export const ResourceItem = ({ item, action, continueAction, isSelected, hasAlert = false, role }: IProps) => {
    const intl = useIntl();
    return (
        <Card isSelected={isSelected} action={!isSelected ? () => action(String(item.pk)) : () => ({})}>
            <div className="info">
                {item.image_link && (
                    <img
                        className="resourceImage"
                        src={item.image_link}
                        alt={intl.formatMessage({ id: 'ALT_RESOURCE_IMAGE' })}
                    />
                )}
                <div>
                    <InfoItem
                        label={intl.formatMessage({
                            id:
                                role === RoleType.Maker
                                    ? hasAlert
                                        ? 'LABEL_RESOURCE'
                                        : 'LABEL_MATERIAL_TYPE'
                                    : 'LABEL_RESOURCE',
                        })}
                        text={item.name}
                    />
                </div>

                <InfoItem label={intl.formatMessage({ id: 'LABEL_ID' })} text={String(item.pk)} />
                {isSelected && (
                    <Button type="itemButton primary actonButton" action={() => continueAction()}>
                        <FontAwesomeIcon size="lg" icon={faArrowRight} />
                    </Button>
                )}
            </div>

            {isSelected && hasAlert && (
                <motion.div
                    className="instructions"
                    initial="collapsed"
                    animate="open"
                    exit="collapsed"
                    variants={{
                        open: { opacity: 1, height: 'auto' },
                        collapsed: { opacity: 0, height: 0 },
                    }}
                    transition={{ duration: 0.3, ease: [0.04, 0.62, 0.23, 0.98] }}
                >
                    <div>{intl.formatMessage({ id: 'LABEL_IMPORTANT' }).toUpperCase()}</div>
                    <div className="margin-s"></div>
                    <span>{intl.formatMessage({ id: 'RESOURCES_PDF_MESSAGE_1' })}</span>
                    <div className="margin-s"></div>
                    <div className="pdfLink button full">
                        <a href={item.howto_link} target="_blank" rel="noopener noreferrer">
                            {intl.formatMessage({ id: 'LABEL_PDF' })}
                        </a>
                    </div>
                    {item.spl_link && (
                        <div className="pdfLink button full">
                            <a href={item.spl_link} target="_blank" rel="noopener noreferrer">
                                {intl.formatMessage({ id: 'LABEL_STL' })}
                            </a>
                        </div>
                    )}
                </motion.div>
            )}
            <div className="margin-s"></div>
        </Card>
    );
};
