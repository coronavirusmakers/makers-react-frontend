import React from 'react';
import { IInventory, ResourceType, RoleType } from '../../../models';
import { Card } from '../index';
import { InfoItem } from '../../infoItem';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import { UrlManager } from '../../../helpers/helpers';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQrcode } from '@fortawesome/free-solid-svg-icons';

interface IProps {
    item: IInventory;
    resourceType: ResourceType;
}

export enum InventoryStatusType {
    Created = 'Creado',
    Waiting = 'En Tránsito',
    Enviado = 'Recogido',
}
export const InventoryItem = ({ item, resourceType }: IProps) => {
    const intl = useIntl();
    const history = useHistory();
    const goToSummary = (inventory: IInventory) => {
        history.push(UrlManager(RoleType.Maker).success, {
            data: inventory,
        });
    };

    const getInventoryStatus = (): InventoryStatusType => {
        if (item.shippings_object.length > 0) {
            let isSended = item.shippings_object.find((item) => item.status >= 1 && item.status !== 5);
            let isCreated = item.shippings_object.length === 0;
            if (!!isSended) {
                return InventoryStatusType.Enviado;
            }

            if (!!isCreated) {
                return InventoryStatusType.Created;
            }
            let hasWaitingShipping =
                item.shippings_object.length > 1 ? false : item.shippings_object.filter((item) => item.status === 0);
            if (hasWaitingShipping && hasWaitingShipping.length > 0) {
                return InventoryStatusType.Created;
            }
            return InventoryStatusType.Waiting;
        }
        return InventoryStatusType.Created;
    };
    return (
        <Card>
            <div className="info">
                <InfoItem
                    label={intl.formatMessage({
                        id: resourceType === ResourceType.FinalProduct ? 'LABEL_INVENTORY' : 'LABEL_MATERIAL_ORDER',
                    })}
                    text={`${item.pk}`}
                />
                <InfoItem
                    label={intl.formatMessage({
                        id: resourceType === ResourceType.FinalProduct ? 'LABEL_RESOURCE' : 'LABEL_MATERIAL_TYPE',
                    })}
                    text={item.resource_object.name}
                />
                <div className="downloadBarcode" onClick={() => goToSummary(item)}>
                    <FontAwesomeIcon size="lg" icon={faQrcode} className="barcode" />
                </div>
            </div>
            <div className="info">
                <InfoItem label={intl.formatMessage({ id: 'LABEL_QUANTITY' })} text={String(item.quantity)} />
                <InfoItem
                    label={intl.formatMessage({ id: 'LABEL_DATE' })}
                    text={`${moment(item.created_date).format('DD/MM/YYYY')}`}
                />
                <InfoItem label={intl.formatMessage({ id: 'LABEL_STATUS' })} text={getInventoryStatus()} />
            </div>
            <div className="margin-s"></div>
        </Card>
    );
};
