import React from 'react';
import './index.scss';
interface IProps {
    options: { pk: number; name: string }[];
    name: string;
    label: string;
    value?: string;
    onChange: (e: any) => void;
    placeholder: string;
    required?: boolean;
    error?: string;
    classList?: string;
}

export const SelectInput = ({
    options,
    name,
    label,
    value = '',
    onChange,
    placeholder,
    required = false,
    error,
    classList = '',
}: IProps) => {
    return (
        <div className={`formGroup ${classList}`}>
            <label htmlFor={name}>{label}</label>
            <select value={value} autoComplete="off" onChange={(e) => onChange(e)} required={required}>
                <option value="" disabled>
                    {placeholder}
                </option>

                {options.length > 0 &&
                    options.map((option) => (
                        <option key={option.pk} value={option.pk}>
                            {option.name}
                        </option>
                    ))}
            </select>
            {error && (
                <div className="error">
                    <span>{error}</span>
                </div>
            )}
        </div>
    );
};
