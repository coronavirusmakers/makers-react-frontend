import React from 'react';
import './index.scss';
interface IProps {
    value: boolean;
    label: string;
    onChange: () => void;
}
export const CheckBox = ({ value, label, onChange }: IProps) => {
    return (
        <div className="formGroup">
            <label className="checkboxLabel">
                {label}
                <input type="checkbox" checked={value} onChange={() => onChange()} />
                <span className="checkmark"></span>
            </label>
        </div>
    );
};
