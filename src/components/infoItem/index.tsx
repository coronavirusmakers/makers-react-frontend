import React from "react"
import "./index.scss"
interface IResourceItem { label: string, text: string }


export const InfoItem = ({ label, text }: IResourceItem) => (
    <div className="infoItem">
        <div className="label"> {label}</div>
        <div className="text"> {text}</div>
    </div>
)
