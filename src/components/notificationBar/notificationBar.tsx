import React, { useRef } from 'react';
import './notificationBar.scss';
import { motion, AnimatePresence } from 'framer-motion';

interface IProps {
    color: 'red' | 'green';
    notificationMessage: string;
}
export const NotificationBar = ({ color, notificationMessage }: IProps) => {
    const ref: any = useRef(null);

    const variants = {
        hidden: {
            height: 0,
            opacity: 0,
        },
        visible: {
            transition: {
                delay: 0.5,
            },
            zIndex: 600,
            height: '60px',
            opacity: 1,
        },
    };
    return (
        <AnimatePresence>
            <motion.div
                ref={ref}
                id="notificationBar"
                initial="hidden"
                animate={notificationMessage !== '' ? 'visible' : 'hidden'}
                exit="hidden"
                variants={variants}
                className={`progressBarContainer ${color}`}
            >
                {!!notificationMessage && <p id="notificationText">{notificationMessage}</p>}
            </motion.div>
        </AnimatePresence>
    );
};
