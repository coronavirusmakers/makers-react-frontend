import React, { useState } from 'react';
import { IPerson } from '../models';

interface IContext {
    person: IPerson | null;
    setPerson: (value: IPerson | null) => void;
    getPersonPk: () => number | null;
}

const initialState: IContext = {
    person: null,
    setPerson: (value: IPerson | null) => ({}),
    getPersonPk: () => null,
};

export const UserDataContext = React.createContext(initialState);

export const UserDataProvider = (props: any) => {
    const [person, setPerson] = useState<IPerson | null>(null);

    const getPersonPk = (): number | null => (person ? person.pk : null);

    return (
        <UserDataContext.Provider
            value={{
                person,
                setPerson,
                getPersonPk,
            }}
        >
            {props.children}
        </UserDataContext.Provider>
    );
};
