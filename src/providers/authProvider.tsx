import React from 'react';
interface IContext {
    accessToken: string | null;
    refreshToken: string | null;
    setAccessToken: (value: string | null) => void;
    setRefreshToken: (value: string | null) => void;
    isAuthenticated: () => boolean;
    logout: () => void;
}

const initialState: IContext = {
    accessToken: null,
    refreshToken: null,
    setAccessToken: (value: string | null) => ({}),
    setRefreshToken: (value: string | null) => ({}),
    isAuthenticated: () => false,
    logout: () => ({}),
};

export const AuthContext = React.createContext(initialState);
export const AuthConsumer: any = AuthContext.Consumer;

export class AuthProvider extends React.Component {
    public authEntity: any;

    private constructor(props: any) {
        super(props);
        this.authEntity = new AuthEntity();
    }

    public render() {
        return <AuthContext.Provider value={this.authEntity}>{this.props.children}</AuthContext.Provider>;
    }
}

class AuthEntity {
    public accessToken: string | null;
    public refreshToken: string | null;

    constructor() {
        this.accessToken = localStorage.getItem('access');
        this.refreshToken = localStorage.getItem('refresh');
    }

    public setAccessToken = (value: string | null) => {
        if (value === null) {
            value = '';
        }
        localStorage.setItem('access', value);
        this.accessToken = value;
    };
    public setRefreshToken = (value: string | null) => {
        if (value === null) {
            value = '';
        }
        localStorage.setItem('refresh', value);
        this.refreshToken = value;
    };

    public isAuthenticated = () => {
        return !!this.accessToken;
    };

    public logout = () => {
        this.accessToken = null;
        this.refreshToken = null;
        localStorage.clear();
    };
}
