import React from 'react';
import { useIntl } from 'react-intl';
import { Layout } from '../../layout';
import { Button } from '../../../components/button';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Spinner } from '../../../components/spinner';
import { RequestItem } from '../../../components/card/requestItem';
import { RoleType } from '../../../models';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

interface IProps {
    roleType: RoleType;
    pageTitle: string;
    action: () => void;
    requests: any[];
    getNext: () => void;
    nextUrl: string | null;
}
export const MainRequestContainer = ({ pageTitle, action, requests, getNext, nextUrl, roleType }: IProps) => {
    const intl = useIntl();

    return (
        <Layout>
            <div id="list">
                <div className="header">
                    <h2>{pageTitle}</h2>
                    <Button type="rounded-desk primary" action={() => action()}>
                        <FontAwesomeIcon size="lg" icon={faPlus} />
                    </Button>
                </div>
                <InfiniteScroll
                    dataLength={requests.length}
                    next={() => getNext()}
                    hasMore={!!nextUrl}
                    scrollThreshold="600px"
                    scrollableTarget="list"
                    endMessage={<p className="noMorePost">{intl.formatMessage({ id: 'LABEL-LIST-FINISHED' })}</p>}
                    loader={
                        <div className="spinerBlock">
                            <Spinner />
                        </div>
                    }
                >
                    {requests.map((item) => (
                        <RequestItem key={item.pk} item={item} roleType={roleType} />
                    ))}
                </InfiniteScroll>
                <Button type="rounded primary" action={() => action()}>
                    <FontAwesomeIcon size="lg" icon={faPlus} />
                </Button>
            </div>
        </Layout>
    );
};
