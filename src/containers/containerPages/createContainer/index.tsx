import React from 'react';
import { Layout } from '../../../containers/layout';
import { ResourceItem } from '../../../components/card/resourceItem';
import { useIntl } from 'react-intl';
import { IResource, RoleType } from '../../../models';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Spinner } from '../../../components/spinner';

interface IProps {
    resources: IResource[];
    setSelected: (id: string) => void;
    selected: string;
    label: string;
    showQuantityPopup: () => void;
    getNext: () => void;
    nextUrl: string | null;
    hasAlertInItems: boolean;
    role: RoleType;
}
export const CreateContainer = ({
    resources,
    setSelected,
    selected,
    label,
    showQuantityPopup,
    getNext,
    nextUrl,
    hasAlertInItems,
    role,
}: IProps) => {
    const intl = useIntl();

    return (
        <Layout>
            <div className="header fixed">
                <h2>{intl.formatMessage({ id: label })}</h2>
            </div>
            <InfiniteScroll
                dataLength={resources.length}
                next={() => getNext()}
                hasMore={!!nextUrl}
                scrollThreshold="600px"
                scrollableTarget="list"
                endMessage={<p className="noMorePost">{intl.formatMessage({ id: 'LABEL-LIST-FINISHED' })}</p>}
                loader={
                    <div className="spinerBlock">
                        <Spinner />
                    </div>
                }
            >
                {resources.map((item) => (
                    <ResourceItem
                        role={role}
                        hasAlert={hasAlertInItems}
                        isSelected={String(item.pk) === selected}
                        action={(id) => setSelected(id)}
                        continueAction={() => showQuantityPopup()}
                        key={item.pk}
                        item={item}
                    />
                ))}
            </InfiniteScroll>
        </Layout>
    );
};
