import React from 'react';
import { Button } from '../../../components/button';
import { useIntl } from 'react-intl';
interface IProps {
    onConfirm: () => void;
    text: string;
}
export const AdviceModal = ({ onConfirm, text }: IProps) => {
    const intl = useIntl();

    return (
        <div className="modalLimit">
            <h3>{intl.formatMessage({ id: text })}</h3>
            <Button type="primary" action={() => onConfirm()}>
                {intl.formatMessage({ id: 'SUMMARY_INVENTORY_PAGE_GO_TO_MAIN' })}
            </Button>
        </div>
    );
};
