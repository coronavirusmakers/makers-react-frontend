import React, { useState } from 'react';
import { Input } from '../../../components/input';
import { Button } from '../../../components/button';
import { useIntl } from 'react-intl';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import useNotification from '../../../customHooks/useNotification';

interface IProps {
    onConfirm: (quantity: number) => void;
    isInventory?: boolean;
}
export const QuantityModal = ({ onConfirm, isInventory = true }: IProps) => {
    const [quantity, setQuantity] = useState('');
    const intl = useIntl();
    const { setNotificationMessageDelayed } = useNotification();

    const manageQuantity = (e: any) => {
        let { value } = e.target;
        setQuantity(value);
    };

    const createInventory = () => {
        if (parseInt(quantity) > 1000 || parseInt(quantity) <= 0) {
            setNotificationMessageDelayed('red', 'LABEL_MAX_QUANTITY');
        } else {
            onConfirm(parseInt(quantity));
        }
    };

    return (
        <div className="modal">
            <Input
                name="quantity"
                value={quantity}
                type="number"
                label={intl.formatMessage({ id: !isInventory ? 'LABEL_QUANTITY' : 'LABEL_QUANTITY_FAB' })}
                onChange={(e) => manageQuantity(e)}
                placeholder="0"
            />
            <Button
                type="rounded-desk primary"
                isDisabled={quantity === ''}
                action={quantity !== '' ? () => createInventory() : () => null}
            >
                <FontAwesomeIcon size="lg" icon={faCheck} />
            </Button>
            <Button
                type="rounded primary"
                isDisabled={quantity === ''}
                action={quantity !== '' ? () => createInventory() : () => null}
            >
                <FontAwesomeIcon size="lg" icon={faCheck} />
            </Button>
        </div>
    );
};
