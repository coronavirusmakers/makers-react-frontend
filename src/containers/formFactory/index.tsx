import React, { useState, useEffect } from 'react';
import './index.scss';
import { Input } from '../../components/input';
import { RegionInput } from '../../components/regionInput';
import { SelectInput } from '../../components/selectInput';

import { useIntl } from 'react-intl';
import { Button } from '../../components/button';
import { motion, AnimatePresence } from 'framer-motion';

const Components = {
    Input,
    SelectInput,
    RegionInput,
};

type ComponentType = 'Input' | 'SelectInput' | 'RegionInput';
export interface IInput {
    name: string;
    type: string;
    label: string;
    placeholder: string;
    component: ComponentType;
    required?: boolean;
    options?: { pk: number; name: string }[];
    pattern?: string;
    classList?: string;
    defaultValue?: any;
    readOnly?: boolean;
}
interface IProps {
    items: IInput[];
    submitAction?: (formState: any) => Promise<any>;
    submitLabel: string;
    children: any;
    defaultValues?: { [key: string]: any } | null;
}

export const FormFactory = ({ items, children, submitAction, submitLabel, defaultValues }: IProps) => {
    const intl = useIntl();
    const [formValues, setformValues] = useState<{ [key: string]: any }>({});

    const [errors, setErrors] = useState<{ [key: string]: any }>({
        non_field_errors: '',
    });

    const variants = {
        hidden: {
            opacity: 0,
            transition: {
                duration: 0.3,
                ease: 'easeOut',
            },
        },
        visible: {
            opacity: 1,
            transition: {
                duration: 0.3,
                ease: 'easeOut',
            },
        },
    };

    useEffect(() => {
        let isSubscribed = true;
        if (!!defaultValues && isSubscribed) {
            setformValues(defaultValues);
        }
        return () => {
            isSubscribed = false;
        };
    }, [defaultValues]);

    const submitForm = (e: any) => {
        e.preventDefault();
        if (!!submitAction) {
            submitAction(formValues).catch((e) => {
                if (e && e.data) {
                    let errorsResult: any = {};
                    Object.keys(e.data).forEach((key: any) => {
                        errorsResult[key] = e.data[key][0];
                    });
                    setErrors(errorsResult);
                }
            });
        }
    };

    const onInputChange = (e: any, name: string, component: ComponentType) => {
        if (component === 'RegionInput') {
            onRegionChange(e);
            return;
        }

        const { value } = e.target;
        setformValues((current) => ({ ...current, ...{ [name]: value } }));
    };

    const onRegionChange = (e: any) => {
        const { region, city, cp } = e.target;
        setformValues((current) => ({ ...current, ...{ region, city, cp } }));
    };

    return (
        <AnimatePresence initial={false}>
            <motion.div
                className="formFactory"
                animate={'visible'}
                initial={'hidden'}
                exit="hidden"
                variants={variants}
            >
                {children}
                <form className="form" onSubmitCapture={(e) => submitForm(e)}>
                    {items.map((input) => {
                        let Component = Components[input.component];
                        return (
                            <Component
                                key={input.name}
                                classList={input.classList}
                                name={input.name}
                                value={formValues[input.name] !== null ? formValues[input.name] : ''}
                                type={input.type}
                                defaultValue={input.defaultValue}
                                pattern={input.pattern}
                                required={input.required}
                                error={errors[input.name]}
                                readOnly={input.readOnly}
                                label={intl.formatMessage({ id: input.label })}
                                onChange={(e) => onInputChange(e, input.name, input.component)}
                                placeholder={intl.formatMessage({ id: input.placeholder })}
                                options={input.options || []}
                            />
                        );
                    })}
                    {errors.non_field_errors !== '' && (
                        <div className="error">
                            <span>{errors.non_field_errors}</span>
                        </div>
                    )}

                    <div className="margin-m" />
                    {!!submitAction && (
                        <Button type="primary submit" action={() => ({})}>
                            {intl.formatMessage({ id: submitLabel })}
                        </Button>
                    )}
                </form>
            </motion.div>
        </AnimatePresence>
    );
};
