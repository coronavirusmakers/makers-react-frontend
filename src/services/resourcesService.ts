import { dataSource } from './dataSource';
import { ResourceType, IPaginatedResponse, IResource } from '../models';
export const ResourceService = () => {
    const getAllFromType = (type: ResourceType): Promise<IPaginatedResponse<IResource>> => {
        return dataSource({
            method: 'GET',
            url: `/logistica/basic/resource/`,
            params: {
                resource_type: type,
            },
        });
    };

    return {
        getAllFromType,
    };
};
