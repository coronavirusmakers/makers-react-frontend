import { dataSource } from './dataSource';
import { ResourceType, IPaginatedResponse, IInventory } from '../models';

export interface IPost {
    resource: number;
    quantity: number;
}

export const InventoryService = () => {
    const getInventoryByPersonId = (id: number, typeToFetch: ResourceType): Promise<IPaginatedResponse<IInventory>> => {
        return dataSource({
            method: 'GET',
            url: `/logistica/basic/person/${id}/inventory/?ordering=-created_date`,
            params: {
                resource_type: typeToFetch,
                limit: 10,
            },
        });
    };

    const createInventory = (data: IPost, id: number): Promise<IInventory> => {
        return dataSource({
            config: {
                handleError: true,
            },
            data,
            method: 'POST',
            url: `/logistica/basic/person/${id}/inventory/`,
        });
    };

    const deleteInventory = (personId: number, inventoryId: number): Promise<IInventory> => {
        return dataSource({
            config: {
                handleError: true,
            },
            method: 'DELETE',
            url: `/logistica/basic/person/${personId}/inventory/${inventoryId}/`,
        });
    };

    const updateShippingStatus = (personPk: number, shippingId: number, status: number): Promise<IInventory> => {
        return dataSource({
            config: {
                handleError: true,
            },
            data: {
                status,
            },
            method: 'PATCH',
            url: `/logistica/basic/person/${personPk}/shipping/${shippingId}/`,
        });
    };

    return {
        getInventoryByPersonId,
        createInventory,
        updateShippingStatus,
        deleteInventory,
    };
};
