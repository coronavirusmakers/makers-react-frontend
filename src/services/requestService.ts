import { dataSource } from './dataSource';
import { IPaginatedResponse, IRequest } from '../models';
import { IPost } from './inventoryService';

export const RequestService = () => {
    const getRequestByPersonId = (id: number): Promise<IPaginatedResponse<IRequest>> => {
        return dataSource({
            method: 'GET',
            url: `/logistica/basic/person/${id}/request/?ordering=-created_date`,
            params: {
                limit: 10,
            },
        });
    };

    const createRequest = (data: IPost, id: number): Promise<IRequest> => {
        return dataSource({
            config: {
                handleError: true,
            },
            data,
            method: 'POST',
            url: `/logistica/basic/person/${id}/request/`,
        });
    };

    const cancelRequest = (personId: number, id: number): Promise<IRequest> => {
        return dataSource({
            config: {
                handleError: true,
            },
            data: {
                status: 4,
            },
            method: 'PATCH',
            url: `/logistica/basic/person/${personId}/request/${id}/`,
        });
    };

    return {
        getRequestByPersonId,
        createRequest,
        cancelRequest,
    };
};
