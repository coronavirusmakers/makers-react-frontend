import { dataSource } from './dataSource';

export const RegionService = () => {
    const getRegions = (csv_search?: string) => {
        return dataSource({
            method: 'GET',
            url: '/logistica/basic/region',
            params: {
                csv_search,
            },
        });
    };

    return {
        getRegions,
    };
};
