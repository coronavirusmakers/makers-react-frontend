import React, { useEffect, useState } from 'react';
import './index.scss';
import { useHistory } from 'react-router-dom';
import { ResourceService } from '../../../services/resourcesService';
import { useModal } from '../../useModal';
import { QuantityModal } from '../../../containers/modal/quantityModal';
import { IPost } from '../../../services/inventoryService';
import { useUserData } from '../../useUserData';
import { AdviceModal } from '../../../containers/modal/adviceModal/adviceModal';
import { ResourceType, IResource, RoleType } from '../../../models';
import { dataSource } from '../../../services/dataSource';
import { EntityType, UrlManager } from '../../../helpers/helpers';
interface IProps {
    typeToFetch: ResourceType;
    entity: EntityType;
    role: RoleType;
    createAction: (data: IPost, id: number) => Promise<any>;
}

export const useCreatePage = ({ typeToFetch, entity, role, createAction }: IProps) => {
    const { getPersonPk } = useUserData();
    const [resources, setResources] = useState<IResource[]>([]);
    const [selected, setSelected] = useState<string>('');
    const { setShowModal, resetModal } = useModal();
    const [nextUrl, setNextUrl] = useState<string | null>(null);

    let history = useHistory();

    useEffect(() => {
        let isSubscribed = true;
        ResourceService()
            .getAllFromType(typeToFetch)
            .then((response) => {
                if (isSubscribed) {
                    setNextUrl(response.next);
                    setResources(response.results);
                }
            });
        return () => {
            isSubscribed = false;
        };
    }, [typeToFetch]);

    const createEntity = (quantity: number) => {
        const person_pk = getPersonPk();
        if (person_pk) {
            createAction({ quantity, resource: parseInt(selected) }, person_pk)
                .then((result) => {
                    resetModal();
                    history.push(UrlManager(role, entity).success, {
                        data: result,
                        isJustCreated: true,
                    });
                })
                .catch((e: any) => {
                    if (e.data && e.data.quantity && e.data.quantity.length > 0) {
                        showErrorPopup();
                    } else {
                        showLimitPopup();
                    }
                });
        }
    };

    const showQuantityPopup = () => {
        let modalConfig = {
            active: true,
            content: (
                <QuantityModal
                    onConfirm={(quantity: number) => createEntity(quantity)}
                    isInventory={entity === EntityType.Inventory}
                />
            ),
        };
        setShowModal(modalConfig);
    };

    const showLimitPopup = () => {
        let modalConfig = {
            active: true,
            content: (
                <AdviceModal
                    text="LABEL_LIMIT_REACHED"
                    onConfirm={() => {
                        resetModal();
                        history.push(UrlManager(role, entity).main);
                    }}
                />
            ),
        };
        setShowModal(modalConfig);
    };

    const showErrorPopup = () => {
        let modalConfig = {
            active: true,
            content: (
                <AdviceModal
                    text="LABEL_SOME_ERROR_HAPPEND"
                    onConfirm={() => {
                        resetModal();
                        history.push(UrlManager(role, entity).main);
                    }}
                />
            ),
        };
        setShowModal(modalConfig);
    };

    const getNext = () => {
        if (!!nextUrl) {
            return dataSource({
                method: 'GET',
                baseURL: nextUrl,
            })
                .then((response: any) => {
                    setNextUrl(response.next);
                    setResources((current) => [...current, ...response.results]);
                })
                .catch((err) => console.log(err));
        }
    };

    const selectItem = (id: string) => {
        setSelected((current) => (current !== id ? id : ''));
    };

    return {
        resources,
        setSelected: selectItem,
        selected,
        showQuantityPopup,
        getNext,
        nextUrl,
    };
};
