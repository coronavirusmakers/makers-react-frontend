import { useState, useEffect } from 'react';
import { useUserData } from '../../useUserData';
import './index.scss';
import { IInventory, IRequest } from '../../../models';
import { dataSource } from '../../../services/dataSource';
interface IProps {
    getItems: () => Promise<any>;
}

export const useMainPage = ({ getItems }: IProps) => {
    const { getPersonPk } = useUserData();
    const [list, setList] = useState<IInventory[] | IRequest[]>([]);
    const [nextUrl, setNextUrl] = useState<string | null>(null);
    useEffect(() => {
        let isSubscribed = true;
        if (getPersonPk()) {
            getItems().then((response) => {
                if (isSubscribed) {
                    setNextUrl(response.next);
                    setList(response.results);
                }
            });
        }
        return () => {
            isSubscribed = false;
        };
    }, []);

    const getNext = () => {
        if (!!nextUrl) {
            dataSource({
                method: 'GET',
                baseURL: nextUrl,
            })
                .then((response: any) => {
                    setNextUrl(response.next);
                    setList((current) => [...current, ...response.results]);
                })
                .catch((err) => console.log(err));
        }
    };

    return {
        list,
        getNext,
        nextUrl,
    };
};
