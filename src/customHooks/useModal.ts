import { useContext } from 'react';
import { ModalContext } from '../providers/modalProvider';

export const useModal = () => {
    const { showModal, setShowModal, resetModal } = useContext(ModalContext);

    return {
        showModal,
        setShowModal,
        resetModal,
    };
};
