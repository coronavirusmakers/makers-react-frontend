import { useContext } from 'react';

import { UserDataContext } from '../providers/userDataProvider';

export const useUserData = () => {
    const { person, setPerson, getPersonPk } = useContext(UserDataContext);

    return {
        person,
        setPerson,
        getPersonPk,
    };
};
