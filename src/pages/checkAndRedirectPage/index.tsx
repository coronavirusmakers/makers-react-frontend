import React, { useEffect } from 'react';
import { AuthService } from '../../services/authService';
import { useHistory, RouteProps } from 'react-router-dom';
import { useUserData } from '../../customHooks/useUserData';
import { PersonService } from '../../services/personService';
import { IPerson, RoleType } from '../../models';
import { UrlManager } from '../../helpers/helpers';

export const CheckAndRedirectPage = (props: RouteProps) => {
    const history = useHistory();
    const { setPerson } = useUserData();

    useEffect(() => {
        let jwt = localStorage.getItem('access');

        if (!!jwt) {
            AuthService()
                .verifyJWT({ token: jwt })
                .then(() => {
                    setPersonOnContext();
                })
                .catch((e) => {
                    const refresh = localStorage.getItem('refresh');
                    if (!!refresh) {
                        AuthService()
                            .refreshJWT({ refresh })
                            .then(() => {
                                setPersonOnContext();
                            })
                            .catch((e) => {
                                history.push('/login');
                                localStorage.clear();
                            });
                    }
                });
        } else {
            localStorage.clear();
            history.push('/login');
        }
    }, []);

    const redirectUserToFlow = (current: IPerson) => {
        history.push(UrlManager(current.role).main);
    };

    const setPersonOnContext = () => {
        PersonService()
            .getAllMyPersons()
            .then((response) => {
                if (response.results.length > 0) {
                    let selectedPerson = response.results.find((item: IPerson) => item.role === RoleType.Maker);
                    selectedPerson = !!selectedPerson
                        ? selectedPerson
                        : response.results.find((item: IPerson) => item.role === RoleType.HealthCare);
                    if (!selectedPerson) {
                        history.push('/person');
                    } else {
                        setPerson(selectedPerson);
                        redirectUserToFlow(selectedPerson);
                    }
                } else {
                    history.push('/person');
                }
            });
    };

    return <div></div>;
};
