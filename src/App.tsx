import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { IntlProvider } from 'react-intl';

import { Routes } from './routes/index';
import esTranslations from './translations/es.json';
import { AuthProvider } from './providers/authProvider';
import { NotificationProvider } from './providers/notificationProvider';

const App = () => {
    const language = 'es';
    const translations = {
        es: esTranslations,
    };

    return (
        <AuthProvider>
            <IntlProvider locale={language} messages={translations[language]}>
                <NotificationProvider>
                    <BrowserRouter children={Routes()} basename={'/'} />
                </NotificationProvider>
            </IntlProvider>
        </AuthProvider>
    );
};

export default App;
